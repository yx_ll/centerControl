import vue from '../../main'
import { Loading } from 'element-ui';
import store from '../../store/index'
import { initTopics } from './topics'
import { OBJ, register } from './obj'
import { displayMap } from './map'
import { showWayPoints } from './waypoints'
import { tabLocalPlan } from './path'
import {event_util} from './event_util'
export let ros, viewer, zoomview;


//初始化ros
function rosInit() {
    return new Promise(function (resolve, reject) {
        var url = store.state.centerControlIP;
        // var url = '10.1.16.233';
        ros = new ROSLIB.Ros({
            url: 'ws://' + url + ':9090',
            groovyCompatibility: true
        });

        //链接到websocket服务器
        ros.on('connection', () => {
            console.log(`[INFO]Connected to rosbridge ${store.state.centerControlIP}.`);
            resolve('ros Connected');
            let msg = {
                text: `Connected to rosbridge ${store.state.centerControlIP}`,
                type: "success",
                time: vue.getDate()
            }
            vue.$debugout.log(msg)
            console.log(vue.$debugout.getLog(msg))
            store.commit("setCenterLogs", msg)
        });
        //关闭websocket服务器
        ros.on('close', () => {
            console.log(`[INFO]Rosbridge server ${url} closed.`);
            let msg = {
                text: `Rosbridge server ${url} closed`,
                type: "error",
                time: vue.getDate()
            }
            vue.$debugout.log(msg)
            console.log(vue.$debugout.getLog(msg))
            store.commit("setCenterLogs", msg);
            reject('Rosbridge连接已关闭')
        });
        //关闭websocket服务器出错
        ros.on('error', () => {
            let msg = {
                text: `Error connecting to websocket server`,
                type: "error",
                time: vue.getDate()
            }
            vue.$debugout.log(msg)
            console.log(vue.$debugout.getLog(msg))
            store.commit("setCenterLogs", msg);
            reject('Rosbridge连接失败')
        });

        /**
         * 初始化机器人列表
         */
        var robotsTopic = new ROSLIB.Topic({
            ros: ros,
            name: '/diagnostics_agg',
            messageType: 'diagnostic_msgs/DiagnosticArray'
        });
        var robots = ["/agv_0","/agv_1","/agv_2","/agv_3"]
        var robotsObject = {
            info:{}
        };
        var robotsArr = []

        var robotsOBJ = Object.create(null)
        robotsTopic.subscribe((msg)=>{
            // console.log(msg)
            robots.forEach((robot)=>{
                msg.status.map((item, index)=>{
                    switch (item.name) {
                        case robot:
                            Object.assign(robotsObject, {name: robot, info: msg.status[index]});
                            break;
                        case robot + '/ALGORITHM':
                            Object.assign(robotsObject.info, {ALGORITHM: msg.status[index]});
                            break;  
                    }
                })
                // console.log(robotsObject)
                robotsArr.push(robotsObject);
                robotsArr = [...new Set(robotsArr)];
                store.commit("setRobotsInfo", robotsArr)
            })
            
            
        })
    })
}

function listenTf() {
    return new Promise(function (resolve, reject) {
        //从ros中订阅TFS
        var tfClient = new ROSLIB.TFClient({
            ros: ros,
            fixedFrame: 'map',
            angularThres: 0.01,
            transThres: 0.01
        });
        tfClient.subscribe('odom', (tf) => {
            OBJ.centerTfMsg['map2odom'] = {
                header: {
                    stamp: null
                },
                transform: tf
            };
        });
        tfClient.subscribe('base_footprint', (tf) => {
            OBJ.centerTfMsg['map2base_footprint'] = {
                header: {
                    stamp: null
                },
                transform: tf
            };
        });
        tfClient.subscribe('base_laser', (tf) => {
            OBJ.centerTfMsg['map2base_laser'] = {
                header: {
                    stamp: null
                },
                transform: tf
            };
        });
        var tfClient2 = new ROSLIB.TFClient({
            ros: ros,
            fixedFrame: 'base_link',
            angularThres: 0.01,
            transThres: 0.01
        });
        tfClient2.subscribe('base_laser', (tf) => {
            OBJ.centerTfMsg['base_link2base_laser'] = {
                header: {
                    stamp: null
                },
                transform: tf
            };
        });
        resolve('tfClient')
    })
}

//initStage
function initStage() {
    return new Promise(function (resolve, reject) {
        var screenWidth = window.innerWidth || document.body.clientWidth || document.documentElement.clientWidth;
        var screenHeight = window.innerHeight || document.body.clientHeight || document.documentElement.clientHeight;
        var rMap = screenWidth / screenHeight;//比例
        var width = screenWidth;
        var height = screenHeight;
        if (screenWidth > screenHeight) {
            width = screenHeight * rMap;
            height = screenHeight;
        } else {
            width = screenWidth;
            height = screenWidth / rMap;
        }

        OBJ.centerMapInfo = {
            windowWidth: width,
            windowHeight: height
        };

        // Create the main viewer.
        viewer = new ROS2D.Viewer({
            divID: 'MAP',
            width: OBJ.centerMapInfo.windowWidth,
            height: OBJ.centerMapInfo.windowHeight
        });
        zoomview = new ROS2D.ZoomView({
            rootObject: viewer.scene
        });

        //滚轮放大缩小
        event_util.addHandler(document, "mousewheel", function (event) {
            event = event_util.getEvent(event);
            if (event.layerX > 1000 || event.layerY > 1000) {
                return false;
            }
            if (parseFloat(event.wheelDelta) > 0) {
                zoomview.startZoom(event.layerX, event.layerY);
                zoomview.zoom(1.2);
            } else {
                zoomview.startZoom(event.layerX, event.layerY);
                zoomview.zoom(0.8);
            }
        });

        //实例化画布对象
        var stage = viewer.scene;
        createjs.Touch.enable(stage);//开启触摸，disable禁止触摸
        createjs.Ticker.setFPS(25);//一秒25帧


        /**
         * 拖拽移动画布
         * **/
        stage.enableMouseOver(10);//用到mouseover要加上这一句


        createjs.Touch.enable(stage);//移动端也支持点击移动事件，允许设备触控

        stage.mouseMoveOutside = true;//鼠标离开画布继续调用鼠标移动事件

        stage.on("mousedown", function (evt) {
            if (store.state.disabledMoveMap) {
                return
            } else {
                // keep a record on the offset between the mouse position and the container position. currentTarget will be the container that the event listener was added to:
                evt.currentTarget.offset = { x: this.x - evt.stageX, y: this.y - evt.stageY };
            }
        });
        stage.on("pressmove", function (evt) {
            if (store.state.disabledMoveMap) {
                return
            } else {
                // Calculate the new X and Y based on the mouse new position plus the offset.
                evt.currentTarget.x = evt.stageX + evt.currentTarget.offset.x;
                evt.currentTarget.y = evt.stageY + evt.currentTarget.offset.y;
                // make sure to redraw the stage to show the change:
                stage.update();
            }
        });
        stage.update();

        //刻度事件，实时监听变化
        createjs.Ticker.addEventListener('tick', function () {
            //更新，重新绘制画布
            stage.update();
        });
        resolve('initStage success')
    })
}

//首先进行加载
export function loadCenterControl(ip){
    store.commit("setCenterIP", ip)
    // Loading.service({
    //     text: "Loading"
    // })
    async function queue(arr) {
        let res = [];
        for (let fn of arr) {
            let data = await fn();
            res.push(data)
        }
        return await res;
    }
    queue([register, rosInit, listenTf, initTopics, initStage, displayMap, showWayPoints, tabLocalPlan])
        .then(data => {
            console.log(data);
            Loading.service().close();
        })
        .catch(data => {
            console.log(data)
            Loading.service().close();
        })
}

