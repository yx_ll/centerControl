import store from '../../store/index'
import { viewer, zoomview } from './centerControl'
import { OBJ } from './obj'
import { robotMarker } from './robots'



//缩放
export function mapZoomIn(scale) {
    console.log("scale")
    let scaleNum = Number(store.state.centerScale);
    store.commit('setCenterScale', scaleNum *= 1.2);
    if (scaleNum > 2.5) {
        store.commit('centerZoomDisabled', 0);
        return
    }
    // showWayPoints2();
    // robotMarker();
    zoomview.startZoom(OBJ.centerMapInfo.windowWidth / 2, OBJ.centerMapInfo.windowHeight / 2);
    zoomview.zoom(1.2);
};

export function mapZoomOut(){
    console.log("out")
    let scaleNum = Number(store.state.centerScale);
    store.commit('setCenterScale', scaleNum *= 0.8);
    if (scaleNum < 0.32) {
        store.commit('centerZoomDisabled', 1);
        return
    }
    console.log(OBJ.centerRobotStage)
    // showWayPoints2();
    // robotMarker();
    zoomview.startZoom(OBJ.centerMapInfo.windowWidth / 2, OBJ.centerMapInfo.windowHeight / 2);
    zoomview.zoom(0.8);
}
