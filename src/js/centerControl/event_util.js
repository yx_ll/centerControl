var event_util = {
    //添加事件
    addHandler: function(element, type, handler) {
        if(element.addEventListener) {                         
            element.addEventListener(type, handler, false);    
        } else if(element.attachEvent) {                       
            element.attachEvent("on" + type, handler);
        } else {
            element["on" + type] = handler;                    
        }
    },
    //移除事件
    removeHandler: function(element, type, handler) {
        if(element.removeEventListener) {
            element.removeEventListener(type, handler, false);
        } else if(element.detachEvent) {
            element.detachEvent("on" + type, handler);
        } else {
            element["on" + type] = null;
        }
    },
    //获取事件对象
    getEvent: function(event) {
        return event ? event : window.event;
    },
    //获取事件目标
    getTarget: function(event) {
        return event.target || event.srcElement;
    },
    //阻止事件冒泡
    stopPropagation: function(event) {
        if(event.stopPropagation) {
            event.stopPropagation();
        } else {
            event.cancelBubble = true;
        }
    },
    //取消事件默认行为
    preventDefault: function(event) {
        if(event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    },
    //获取相关元素
    getRelatedTarget: function(event) {
        if(event.relatedTarget) {
            return event.relatedTarget;
        } else if(event.fromElement) {
            return event.fromElement;
        } else if(event.toElement) {
            return event.toElement;
        } else {
            return null;
        }
    }
}

export { event_util }