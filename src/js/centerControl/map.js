import store from '../../store/index'
import { OBJ } from './obj'
import { viewer } from './centerControl'
import { robotMarker } from './robots'

export function displayMap(){
    return new Promise(function(resolve, reject){
        OBJ.centerTopic.mapTopic.subscribe(function(msg){
            var imgMap = new ROS2D.ImageMap({
                image: msg.data,
                message: msg.info
            });
            if(OBJ.centerImgMapStage){
                viewer.scene.removeAllChildren();
            }
            OBJ.centerImgMapStage = imgMap;
            viewer.scene.addChild(imgMap);
            let rmap = OBJ.centerMapInfo.windowWidth / imgMap.width;
            viewer.width = OBJ.centerMapInfo.windowWidth;
            viewer.height = imgMap.height * rmap;
            
            if(store.state.rosMode !== "gmapping"){
                viewer.scaleToDimensions(imgMap.width, imgMap.height);
                viewer.shift(imgMap.pose.position.x, imgMap.pose.position.y);
            }else{
                if(store.state.mappingFlag === false){
                    viewer.scaleToDimensions(imgMap.width, imgMap.height);
                    viewer.scene.x = OBJ.centerMapInfo.windowWidth / 2;
                    viewer.scene.y = OBJ.centerMapInfo.windowHeight / 2;
                    store.commit("mappingFlag", true);
                }
            }
            var mapPosition = {
                x: viewer.scene.x,
                y: viewer.scene.y
            }
            OBJ.centerMapXY = mapPosition;
            robotMarker();
            resolve("display map")
        })
    })
}