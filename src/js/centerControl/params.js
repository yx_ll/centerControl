export let PARAMS = {
    CmdEnum: {
        MapSelect: "dbparam-select",  //查询地图
        MapDelete: "dbparam-delete",  //删除地图
        MapUpdate: "dbparam-update", // 切换地图
        MapInsert: "dbparam-insert", // 添加地图
    },
    NavCtrl: {
        stop: 0,
        start: 1,
        pause: 2
    },
    NavCtrlStatus: {
        error: -1,
        idling: 0,
        running: 1,
        paused: 2,//暂停
        completed: 3,
        cancelled: 4
    },
    rosMode: {
        navigation: "navigation",
        gmapping: "gmapping"
    },
    NetworkMode: {
        ap: 'ap',
        wifi: 'wifi'
    }
}