import { OBJ } from './obj'
import store from '../../store/index'
import { viewer } from './centerControl'

export function tabLocalPlan(boole = true) {
    return new Promise(function(resolve, reject){
        store.state.robots.forEach((item, index)=>{
            if (boole) {
                var path = null;
                OBJ.centerTopic[`${item.name}tabLocalPathTopic`].subscribe(function (data) {
                    if (path != null) {
                        viewer.scene.removeChild(path);
                    }
                    if (data.poses.length > 0) {
                        path = new ROS2D.PathShape({
                            path: data,
                            strokeSize: 0.17,
                            strokeColor: createjs.Graphics.getRGB(245, 108, 108, 0.9),
                        });
                        OBJ.centerTabLocalPlanStage[`${item.name}tabLocalPlanStage`] = path;
                        viewer.scene.addChild(path);
                    }
                });
            } else {
                OBJ.centerTopic[`${item.name}tabLocalPathTopic`].unsubscribe();
                viewer.scene.removeChild(OBJ.tabLocalPlanStage[`${item.name}tabLocalPlanStage`]);
            }
            resolve("local plan")
        })
    })
}