import { OBJ } from './obj'
import { viewer } from './centerControl'
import store  from '../../store/index'

export function robotMarker() {
    return new Promise(function (resolve, reject) {
        store.state.robots.forEach((item, index) => {
            if (OBJ.centerRobotStage[`${item.name}robotStage`]) {
                viewer.scene.removeChild(OBJ.centerRobotStage[`${item.name}robotStage`]);
            }
            if(OBJ.centerRobotsName[item.name]){
                viewer.scene.removeChild(OBJ.centerRobotsName[item.name]);
            }
            // marker for the robot
            var robotMarker = new ROS2D.NavigationArrow({
                size: 20,
                strokeSize: 1,
                fillColor: createjs.Graphics.getRGB(255, 255, 153, 0.66),
                pulse: true
            });
            // wait for a pose to come in first
            robotMarker.visible = false;
            viewer.scene.addChild(robotMarker);
            var initScaleSet = false;

            //robot name
            var text = new createjs.Text(`robot:${item.name}`, "12px", "#ff7700");
            text.visible = false;
            text.textAlign = "center";
            viewer.scene.addChild(text)
    
            // setup a listener for the robot pose
            OBJ.centerTopic[`${item.name}robotPoseTopic`].subscribe(function (pose) {
                OBJ.centerRobotPose[`${item.name}robotPose`] = pose;
                // update the robots position on the map
                robotMarker.x = pose.position.x;
                robotMarker.y = -pose.position.y;

                text.x = pose.position.x;
                text.y = -pose.position.y - 0.30;

                if (!initScaleSet) {
                    robotMarker.scaleX = 1.0 / viewer.scene.scaleX;
                    robotMarker.scaleY = 1.0 / viewer.scene.scaleY;

                    text.scaleX = 1.0 / viewer.scene.scaleX;
                    text.scaleY = 1.0 / viewer.scene.scaleY;

                    initScaleSet = true;
                }
                // change the angle
                robotMarker.rotation = viewer.scene.rosQuaternionToGlobalTheta(pose.orientation);
                robotMarker.visible = true;

                text.rotation = viewer.scene.rosQuaternionToGlobalTheta(pose.orientation);
                text.visible = true;

                OBJ.centerRobotStage[`${item.name}robotStage`] = robotMarker;
                OBJ.centerRobotsName[item.name] = text;
                resolve()
            });
        });
    })
}