import vue from '../../main'
import store from '../../store/index'
import { ros } from './centerControl'
import { PARAMS } from './params'
import { OBJ } from './obj'

/**
 * 初始化topics
 */
export function initTopics(){
    return new Promise(function(resolve, reject){

        /**
         * dbparam      地图列表
         * update       更新状态
         */
        var cmdStringTopic = new ROSLIB.Topic({
			ros: ros,
			name: `/cmd_string`,
			messageType: 'std_msgs/String'
        });
        OBJ.centerCmdStringTopic = cmdStringTopic;
		var msg = new ROSLIB.Message({
            data: PARAMS.CmdEnum.MapSelect
        });
        cmdStringTopic.publish(msg);
        setTimeout(() => {
            var feedbackTopic = new ROSLIB.Topic({
                ros:         ros,
                name:        '/shell_feedback',
                messageType: 'std_msgs/String'
            });
            feedbackTopic.subscribe((msg) => {
                switch (msg.data){
                    case '':
                        break;
                    default:
                        var msgs = msg.data.split(/[ :]/);
                        var key  = msgs[0].trim();
                        switch (key){
                            case 'dbparam':
                                console.log("地图列表和名称")
                                // store.commit("mapList", msgs.slice(1));
                                // store.commit('setNowMapName', msgs.slice(1)[0]);
                                break;
                            case 'update':
                                if (msgs[1].trim() === 'success'){
                                    let msg = {
                                        text: `更新成功`,
                                        type: "success",
                                        time: vue.getDate()
                                    }
                                    store.commit("setCenterLogs", msg);
                                }else{
                                    let msg = {
                                        text: `更新失败`,
                                        type: "error",
                                        time: vue.getDate()
                                    }
                                    store.commit("setCenterLogs", msg);
                                }
                                break;
                            default:
                                break;
                        }				
                        break;
                }
            });
        }, 500);

        /**
         * map数据
         */
        var mapTopic = new ROSLIB.Topic({
            ros: ros,
            name: '/map_stream',
            messageType: 'scheduling_msgs/MapStream'
        });
        OBJ.centerTopic['mapTopic'] = mapTopic;

        /**
         * waypoints    站点
         */
        var wpTopic = new ROSLIB.Topic({
            ros: ros,
            name: '/waypoints',
            messageType: 'yocs_msgs/WaypointList'
        });
        OBJ.centerTopic['wpTopic'] = wpTopic;


        /**
         * robots topic
         * 便利所有机器人，获取导航状态
         */
        store.state.robots.forEach(function(item, index){
            var navCtrlStatusTopic = new ROSLIB.Topic({
                ros: ros,
                name: `${item.name}/nav_ctrl_status`,
                messageType: 'yocs_msgs/NavigationControlStatus'
            });
            OBJ.centerTopic[`${item.name}navCtrlStatusTopic`] = navCtrlStatusTopic;
            var taskStateType    = "info";
            var taskStateMessage = "";
            navCtrlStatusTopic.subscribe((message) => {
                OBJ.centerNavCtrlStatus[`${item.name}navCtrlStatus`] = message;
                if(message.status === -1){
                    taskStateType    = "error";
                    taskStateMessage = "设备故障，请先排除故障"
                }else if(message.status === 0){
                    taskStateType    = "info";
                    taskStateMessage = "当前可执行任务"
                }else if(message.status === 1){
                    taskStateType    = "info";
                    taskStateMessage = `正在执行当前任务 ${message.waypoint_name}`
                }else if(message.status === 2){
                    taskStateType    = "info";
                    taskStateMessage = `已暂停当前任务: ${message.waypoint_name}`
                }else if(message.status === 3){
                    taskStateType    = "success";
                    taskStateMessage = `当前任务已完成 ${message.waypoint_name}`
                }else if(message.status === 4){
                    taskStateType    = "success";
                    taskStateMessage = `当前任务已取消 ${message.waypoint_name}`
                }
                let msg = {
                    text: `任务状态: [${item.name}] ${taskStateMessage}`,
                    type: taskStateType,
                    time: vue.getDate()
                }
                store.commit("setCenterLogs", msg);
            });

            /**
             * robot pose
             * 获取所有机器人姿态
             */
            var robotPoseTopic = new ROSLIB.Topic({
                ros: ros,
                name: `${item.name}/robot_pose`,
                messageType: 'geometry_msgs/Pose',
                throttle_rate: 100
            });
            OBJ.centerTopic[`${item.name}robotPoseTopic`] = robotPoseTopic;
            robotPoseTopic.subscribe(function (pose) {
                OBJ.centerRobotPoseMsg[`${item.name}robotPoseMsg`] = pose;
            });

            /**
             * 全局路径
             */
            var globalPathTopic = new ROSLIB.Topic({
                ros: ros,
                name: `${item.name}/move_base/GlobalPlanner/plan`,
                messageType: 'nav_msgs/Path',
            });
            OBJ.centerTopic[`${item.name}globalPathTopic`] = globalPathTopic;

            /**
             * 局部路径
             */
            var tabLocalPathTopic = new ROSLIB.Topic({
                ros: ros,
                name: `${item.name}/move_base/TebLocalPlannerROS/local_plan`,
                messageType: 'nav_msgs/Path',
            });
            OBJ.centerTopic[`${item.name}tabLocalPathTopic`] = tabLocalPathTopic;

            /**
             * rosMode status充电状态和建图状态
             */
            let rosModeTopic = new ROSLIB.Topic({
                ros: ros,
                name: `${item.name}/task_state`,
                messageType: 'diagnostic_msgs/DiagnosticStatus'
            });
            OBJ.centerTopic[`${item.name}rosModeTopic`] = rosModeTopic;
            rosModeTopic.subscribe((msg) => {
                msg.values.map(function (e, i) {
                    if (e.key === 'auto_charge') {
                        switch (e.value) {
                            case '0':
                                // console.log("未充电")
                                break;

                            case '1':
                                console.log("充电中")
                                let msg = {
                                    text: `[${item.name}] 充电中`,
                                    type: 'info',
                                    time: vue.getDate()
                                }
                                store.commit("setCenterLogs", msg);
                                break;
                        }
                    } else if (e.key === 'cartographer') {
                        OBJ.centerTaskState = e.value;
                        if (e.value === "0") {
                            // console.log("navigation 状态")
                        } else if (e.value === "1") {
                            console.log("建图状态")
                            let msg = {
                                text: `[${item.name}] 建图中`,
                                type: 'info',
                                time: vue.getDate()
                            }
                            store.commit("setCenterLogs", msg);
                        } else {

                        }
                    }
                })
            })

            //buttery topic电池状态
            var butteryTopic = new ROSLIB.Topic({
                ros: ros,
                name: `${item.name}/battery`,
                messageType: 'sensor_msgs/BatteryState'
            })
            OBJ.centerTopic[`${item.name}butteryTopic`] = butteryTopic;
            butteryTopic.subscribe(function (msg) {
                if(msg.percentage){
                    let butteryNum = Math.round(msg.percentage * 100);
                    if( 0 <= butteryNum <= 100){
                        if (butteryNum <= 15) {
                            console.log("低电量")
                            // store.commit('setBatteryBg', "#F56C6C");
                            let msg = {
                                text: `[${item.name}] 电量低，请及时充电`,
                                type: 'warning',
                                time: vue.getDate()
                            }
                            store.commit("setCenterLogs", msg);
                        } else {
                            console.log("电量充足")
                            // store.commit('setBatteryBg', "#409EFF");
                        }
                        console.log(`当前电量为${butteryNum}`)
                    }
                    
                }    
            })

            //nav ctrltopic导航目标点
            var navCtrlTopic = new ROSLIB.Topic({
                ros: ros,
                name: `${item.name}/nav_ctrl`,
                messageType: 'yocs_msgs/NavigationControl'
            });
            OBJ.centerTopic[`${item.name}navCtrlTopic`] = navCtrlTopic;

        })

        resolve('init topics')
    })
}