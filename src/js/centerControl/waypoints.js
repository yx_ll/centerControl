import store from '../../store/index'
import { OBJ } from './obj'
import { viewer } from './centerControl'

export function showWayPoints() {
    return new Promise(function (resolve, reject) {
        OBJ.centerTopic.wpTopic.subscribe((message) => {
            OBJ.centerWaypointsMessage = message;
            if (OBJ.centerWaypointsStage) {
                for (var key in OBJ.centerWaypointsStage) {
                    if (OBJ.centerWaypointsStage.hasOwnProperty(key)) {
                        var waypointsContainers = OBJ.centerWaypointsStage[key];
                        var waypointsNames = OBJ.centerWaypointsName[key];
                        for (var i = 0; i < waypointsContainers.length; i++) {
                            viewer.scene.removeChild(waypointsContainers[i]);
                            viewer.scene.removeChild(waypointsNames);
                        }
                    }
                }
            }

            OBJ.centerWayPointsMsg = message;
            var waypointContainers = {
                goal: [],
            };

            //获取到站点信息之后赋给waypointsMsg，渲染站点，添加事件
            for (var i = 0; i < message.waypoints.length; i++) {
                var waypoint = message.waypoints[i];
                var wpInfo = JSON.parse(waypoint.header.frame_id);
                var waypointType = wpInfo.type;

                //如果不是目标站点，直接跳过
                if (waypointType !== 'goal') {
                    continue;
                }

                

                var wpContainer = new ROS2D.NavigationArrow({
                    size: 10,
                    strokeSize: 1,
                    fillColor: createjs.Graphics.getRGB(63, 134, 254, 0.9),
                    pulse: false
                });
                wpContainer.x = waypoint.pose.position.x;
                wpContainer.y = -waypoint.pose.position.y;
                wpContainer.scaleX = 1.0 / viewer.scene.scaleX;
                wpContainer.scaleY = 1.0 / viewer.scene.scaleY;
                // change the angle
                wpContainer.rotation = viewer.scene.rosQuaternionToGlobalTheta(waypoint.pose.orientation);

                //waypoint name
                var waypointName = new createjs.Text(waypoint.name, "10px", "#333333");
                waypointName.visible = false;
                waypointName.x = waypoint.pose.position.x;
                waypointName.y = -waypoint.pose.position.y - 0.2;
                waypointName.scaleX = 1.0 / viewer.scene.scaleX;
                waypointName.scaleY = 1.0 / viewer.scene.scaleY;
                waypointName.textAlign = "center"
                // change the angle
                waypointName.rotation = viewer.scene.rosQuaternionToGlobalTheta(waypoint.pose.orientation);
                waypointName.visible = true;

                wpContainer.name = waypoint.name; // bind name
                wpContainer.frame_id = wpInfo;//站点信息
                waypointContainers[waypointType].push(wpContainer);
                // TODO: handle wp click
                //点击站点导航到站点
                wpContainer.on('click', waypointClick);
                viewer.scene.addChild(wpContainer);
                viewer.scene.addChild(waypointName);
            }
            OBJ.centerWaypointsStage = waypointContainers;
            OBJ.centerWaypointsName = waypointName;
            store.commit('centerWaypoints', waypointContainers.goal);
            resolve('wayPoints success');
        });
    })
}

//click way point
function waypointClick(event) {
    showStationDetail(event.currentTarget.name);
    store.commit("centerCollapseName", "waypoints");//打开站点折叠面板
    store.commit("centerWaypointsActiveName", event.currentTarget.name);//站点的折叠面板
}

/**
 * 点击站点图标或名字
 * 显示站点详情、遮罩，隐藏站点列表
 * **/
export function showStationDetail(fullName) {
    var waypoint = getWaypointByName(fullName);//站点信息
    var wpInfo = JSON.parse(waypoint.header.frame_id);
    if (waypoint) {
        console.log(`[INFO]waypoint:\n-type: ${wpInfo.type};\n-name: ${waypoint.name}.`);
    } else {
        console.warn(`[WARN]Can not find waypoint ${fullName}`);
    }
};

/**
 * 通过名称找到对应站点数据
 * **/
export function getWaypointByName(name) {
    for (var i = 0; i < OBJ.centerWayPointsMsg.waypoints.length; i++) {
        var waypoint = OBJ.centerWayPointsMsg.waypoints[i];
        if (waypoint.name === name) {
            return waypoint;
        }
    }
}
