import vue from '../main'
import { Loading, Message, Notification  } from 'element-ui';
import store from '../store/index'
import { ros, OBJ, PARAMS, viewer, zoomview } from './init'

/**
 * listenTF
 * map: 地图坐标，固定坐标系
 * odom: 机器人初始坐标，固定坐标系
 * base_link：机器人中心位置坐标
 * **/
export function listenTf() {
    return new Promise(function (resolve, reject) {
        //从ros中订阅TFS
        var tfClient = new ROSLIB.TFClient({
            ros: ros,
            fixedFrame: 'map',
            angularThres: 0.01,
            transThres: 0.01
        });
        tfClient.subscribe('odom', (tf) => {
            OBJ.tfMsg['map2odom'] = {
                header: {
                    stamp: null
                },
                transform: tf
            };
        });
        tfClient.subscribe('base_footprint', (tf) => {
            OBJ.tfMsg['map2base_footprint'] = {
                header: {
                    stamp: null
                },
                transform: tf
            };
        });
        tfClient.subscribe('base_laser', (tf) => {
            OBJ.tfMsg['map2base_laser'] = {
                header: {
                    stamp: null
                },
                transform: tf
            };
        });
        var tfClient2 = new ROSLIB.TFClient({
            ros: ros,
            fixedFrame: 'base_link',
            angularThres: 0.01,
            transThres: 0.01
        });
        tfClient2.subscribe('base_laser', (tf) => {
            OBJ.tfMsg['base_link2base_laser'] = {
                header: {
                    stamp: null
                },
                transform: tf
            };
        });
        resolve('tfClient')
    })
}

/***
 * 建图函数
 */
export function displayMap(){
    return new Promise(function(resolve, reject){
        OBJ.topic.mapTopic.subscribe(function(msg){
            var imgMap = new ROS2D.ImageMap({
                image: msg.data,
                message: msg.info
            });
            if(OBJ.imgMapStage){
                viewer.scene.removeAllChildren();
            }
            OBJ.imgMapStage = imgMap;
            viewer.scene.addChild(imgMap);
            let rmap = OBJ.mapInfo.windowWidth / imgMap.width;
            viewer.width = OBJ.mapInfo.windowWidth;
            viewer.height = imgMap.height * rmap;
            
            if(store.state.rosMode !== "gmapping"){
                viewer.scaleToDimensions(imgMap.width, imgMap.height);
                viewer.shift(imgMap.pose.position.x, imgMap.pose.position.y);
            }else{
                if(store.state.mappingFlag === false){
                    viewer.scaleToDimensions(imgMap.width, imgMap.height);
                    viewer.scene.x = OBJ.mapInfo.windowWidth / 2;
                    viewer.scene.y = OBJ.mapInfo.windowHeight / 2;
                    store.commit("mappingFlag", true);
                }
            }
            var mapPosition = {
                x: viewer.scene.x,
                y: viewer.scene.y
            }
            OBJ.mapXY = mapPosition;
            robotMarker();
            resolve("display map")
        })
    })
}

/**
 * 点击新建地图
 * seq=1
 * frame_id=salm
 * **/
export function newMapping() {
    if(!verifyNavCtrlStatus()){
        return
    }
    
    Loading.service({
        text: vue.$t("loading")
    });

    //发布启动建图话题,开始打点
    let message = {
        seq: 1,
        frame_id: 'slam'
    }
    var startMessage = new ROSLIB.Message(message);
    var taskSwitchTopic = new ROSLIB.Topic({
        ros: ros,
        name: '/task_switch',
        messageType: 'std_msgs/Header'
    });
    taskSwitchTopic.publish(startMessage);
    console.log('[INFO]Start mapping');
    setTimeout(function () {
        //隐藏添加地图模态框，显示保存地图
        store.commit('switchEditMapBtns', true);
        Loading.service().close();
    }, 2000)
};

/**
 * 点击继续建图
 * seq=2
 * frame_id=salm
 * **/
export function continueMapping() {
    if(!verifyNavCtrlStatus()){
        return
    }

    if (store.state.rosMode === PARAMS.rosMode.gmapping) {
        // Message({
        //     showClose: true,
        //     message: '当前状态为建图状态，请点击保存',
        //     type: 'warning'
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("message.nowMapping"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
            
        return;
    }
    
    Loading.service({
        text: vue.$t("loading")
    })

    //发布启动建图话题,开始打点
    let goonMsg = {
        seq: 2,
        frame_id: 'slam'
    }
    var goonMessage = new ROSLIB.Message(goonMsg)
    var taskSwitchTopic = new ROSLIB.Topic({
        ros: ros,
        name: '/task_switch',
        messageType: 'std_msgs/Header'
    });
    taskSwitchTopic.publish(goonMessage);
    console.log('[INFO]continue mapping');
    setTimeout(function () {
        //显示保存地图按钮
        store.commit('switchEditMapBtns', true);
        Loading.service().close();
    }, 2000)

};

/**
 * 保存并继续建图
 * **/
export function submitSaveMapContinue(mapName){
    let mapNameStr = "";
    Loading.service({
        text: vue.$t("inTheSave")
    });
    if(mapName){
        mapNameStr = "_" + mapName;
    }else{
        mapNameStr = "";
    }
    
    //保存地图
    let saveMsg = {
        seq: 1,
        frame_id: `save_map${mapNameStr}`
    }

    var taskSwitchTopic = new ROSLIB.Topic({
        ros: ros,
        name: '/task_switch',
        messageType: 'std_msgs/Header'
    });
    taskSwitchTopic.publish(new ROSLIB.Message(saveMsg));
    setTimeout(function(){
        Loading.service().close();
        // Message({
        //     showClose: true,
        //     message: '保存成功',
        //     type: 'success'
        // });
        Notification({
            title: vue.$t("titles.success"),
            message: vue.$t("messages.saveSuccess"),
            type: 'success',
            position: 'top-left',
            offset: 100
        });
    }, 2000)
};

/**
 * 保存并关闭建图
 * 保存：seq=1,frame_id=map
 * 关闭建图:seq=0,frame_id=slam
 * **/
export function submitSaveMapBtn(mapName) {
    let mapNameStr = "";
    Loading.service({
        text: vue.$t("inTheSave")
    });
    if(mapName){
        mapNameStr = "_" + mapName;
    }else{
        mapNameStr = "";
    }
    //保存地图
    let saveMsg = {
        seq: 1,
        frame_id: `save_map${mapNameStr}`
    }
    //关闭建图
    let closeMsg = {
        seq: 0,
        frame_id: 'slam'
    }
    var taskSwitchTopic = new ROSLIB.Topic({
        ros: ros,
        name: '/task_switch',
        messageType: 'std_msgs/Header'
    });
    taskSwitchTopic.publish(new ROSLIB.Message(saveMsg));


    //关闭建图
    setTimeout(function () {
        var closeMessage = new ROSLIB.Message(closeMsg);
        taskSwitchTopic.publish(closeMessage);
        setTimeout(function () {
            //关闭保存地图按钮
            store.commit('switchEditMapBtns', false);
            Loading.service().close();
            // Message({
            //     showClose: true,
            //     message: '保存成功',
            //     type: 'success'
            // });
            Notification({
                title: vue.$t("titles.success"),
                message: vue.$t("messages.saveSuccess"),
                type: 'success',
                position: 'top-left',
                offset: 100
            });
        }, 8000)
    }, 1000)
};

//切换地图
export function switchMap(mapName){
    Loading.service({
        text: vue.$t("inTheSwitch")
    })
    let msg = new ROSLIB.Message({
        data: PARAMS.CmdEnum.MapUpdate + ":" + mapName
    });
    OBJ.topic.cmdStringTopic.publish(msg)
};

//删除地图
export function deleteMap(mapName){
    Loading.service({
        text: vue.$t("deleting")
    });
    let msg = new ROSLIB.Message({
        data: PARAMS.CmdEnum.MapDelete + ":" + mapName
    });
    OBJ.topic.cmdStringTopic.publish(msg);
};

//robot定位
export function locationRobot() {
    // zoomview.startZoom(OBJ.imgMapStage.currentImage.x + OBJ.robotStage.x, OBJ.imgMapStage.currentImage.y - OBJ.robotStage.y);
    viewer.scene.x = OBJ.mapXY.x + OBJ.robotStage.x;
    viewer.scene.y = OBJ.mapXY.y + OBJ.robotStage.y;
}

/**
 * 绘制robot
 * 放大缩小时,地图加载时
 * 移除重复的
 * **/
export function robotMarker() {
    return new Promise(function (resolve, reject) {
        if (OBJ.robotStage) {
            viewer.scene.removeChild(OBJ.robotStage);
        }
        // marker for the robot
        var robotMarker = new ROS2D.NavigationArrow({
            size: 20,
            strokeSize: 1,
            fillColor: createjs.Graphics.getRGB(255, 255, 153, 0.66),
            pulse: true
        });
        // wait for a pose to come in first
        robotMarker.visible = false;
        viewer.scene.addChild(robotMarker);
        var initScaleSet = false;

        // setup a listener for the robot pose
        OBJ.topic.robotPoseTopic.subscribe(function (pose) {
            OBJ.robotPose = pose;
            // update the robots position on the map
            robotMarker.x = pose.position.x;
            robotMarker.y = -pose.position.y;
            if (!initScaleSet) {
                robotMarker.scaleX = 1.0 / viewer.scene.scaleX;
                robotMarker.scaleY = 1.0 / viewer.scene.scaleY;
                initScaleSet = true;
            }
            // change the angle
            robotMarker.rotation = viewer.scene.rosQuaternionToGlobalTheta(pose.orientation);
            robotMarker.visible = true;
            OBJ.robotStage = robotMarker;
            resolve()
        });
    })
}


/**
 * show way points
 */
export function showWayPoints() {
    return new Promise(function (resolve, reject) {
        OBJ.topic.wpTopic.subscribe((message) => {
            console.log(message)
            OBJ.waypointsMessage = message;
            if (OBJ.waypointsStage) {
                for (var key in OBJ.waypointsStage) {
                    if (OBJ.waypointsStage.hasOwnProperty(key)) {
                        var waypointsContainers = OBJ.waypointsStage[key];
                        for (var i = 0; i < waypointsContainers.length; i++) {
                            viewer.scene.removeChild(waypointsContainers[i]);
                        }
                    }
                }
            }

            OBJ.wayPointsMsg = message;
            var waypointContainers = {
                goal: [],
            };

            //获取到站点信息之后赋给waypointsMsg，渲染站点，添加事件
            for (var i = 0; i < message.waypoints.length; i++) {
                var waypoint = message.waypoints[i];
                var wpInfo = JSON.parse(waypoint.header.frame_id);
                var waypointType = wpInfo.type;

                //如果不是目标站点，直接跳过
                if (waypointType !== 'goal') {
                    continue;
                }
                var wpContainer = new ROS2D.NavigationArrow({
                    size: 10,
                    strokeSize: 1,
                    fillColor: createjs.Graphics.getRGB(63, 134, 254, 0.9),
                    pulse: false
                });
                wpContainer.x = waypoint.pose.position.x;
                wpContainer.y = -waypoint.pose.position.y;
                wpContainer.scaleX = 1.0 / viewer.scene.scaleX;
                wpContainer.scaleY = 1.0 / viewer.scene.scaleY;
                // change the angle
                wpContainer.rotation = viewer.scene.rosQuaternionToGlobalTheta(waypoint.pose.orientation);

                wpContainer.name = waypoint.name; // bind name
                waypointContainers[waypointType].push(wpContainer);
                // TODO: handle wp click
                //点击站点导航到站点
                wpContainer.on('click', waypointClick);
                viewer.scene.addChild(wpContainer)
            }
            OBJ.waypointsStage = waypointContainers;
            store.commit('stationList', waypointContainers.goal);
            resolve('wayPoints success');
        });
    })
}

//click way point
function waypointClick(event) {
    if(!store.state.userRole){
        return
    }
    showStationDetail(event.currentTarget.name);
}

/**
 * 点击站点图标或名字
 * 显示站点详情、遮罩，隐藏站点列表
 * **/
export function showStationDetail(fullName) {
    var waypoint = getWaypointByName(fullName);//站点信息
    var wpInfo = JSON.parse(waypoint.header.frame_id);
    if (waypoint) {
        hideOtherModule();
        store.commit('wayPointMsg', waypoint);
        store.commit('switchShowMask', true);
        store.commit('switchStationModule', true);
        console.log(`[INFO]waypoint:\n-type: ${wpInfo.type};\n-name: ${waypoint.name}.`);
    } else {
        // Message({
        //     showClose: true,
        //     message: `没有找到${fullName}站点`,
        //     type: 'error',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.error"),
            message: vue.$t("messages.notFindWaypoint") + fullName,
            type: 'error',
            position: 'top-left',
            offset: 100
        });
        console.warn(`[WARN]Can not find waypoint ${fullName}`);
    }
};


/**
 * 急停
 */
export function stopRobot() {
    manualCtrl({
        linear: 0,
        angular: 0
    })
    navCtrl('', 0);
    Notification ({
        title: vue.$t("titles.success"),
        message: vue.$t("messages.taskCanceled"),
        type: 'success',
        position: 'top-left',
        offset: 100
    });
};


/**
 * 导航到指定站点
 * **/
export function navNewStation(fullName) {
    navCtrl(fullName, 1);
};

/**
 * 导航
 * 在0或4空闲状态
 * 不允许发送任务
 * control：0是停止，1是启动
 * **/
export function navCtrl(name, control, ...rest) {
    var allowInterrupt = false;
    if (rest[0] === true) {
        allowInterrupt = true;
    }
    console.log(`[INFO] navstatus ${OBJ.navCtrlStatus.status}`)
    if (!allowInterrupt) {
        if (OBJ.navCtrlStatus.status !== PARAMS.NavCtrlStatus.idling && OBJ.navCtrlStatus.status !== PARAMS.NavCtrlStatus.cancelled) {
            if (control === PARAMS.NavCtrl.start) {//=1
                // Message({
                //     showClose: true,
                //     message: '正在执行其他任务，当前命令将被忽略',
                //     type: 'warning',
                //     duration: 3000
                // });
                Notification({
                    title: vue.$t("titles.warning"),
                    message: vue.$t("messages.tasksBeing"),
                    type: 'warning',
                    position: 'top-left',
                    offset: 100
                });                  
                return;
            }
        }
    }
    var msg = new ROSLIB.Message({
        goal_name: name,
        control: control
    });
    OBJ.topic.navCtrlTopic.publish(msg);
}

/**
 * 显示/隐藏站点列表
 * 先判断有无站点数据
 * 发送站点话题，显示渲染站点
 *  **/
export function showStations(status) {
    if (!OBJ.wayPointsMsg) {
        // Message({
        //     showClose: true,
        //     message: '站点不可用',
        //     type: 'error',
        //     duration: 0
        // });
        Notification({
            title: vue.$t("titles.error"),
            message: vue.$t("messages.waypointUnavailable"),
            type: 'error',
            position: 'top-left',
            offset: 100
        });            
        return;
    }
};


/**
 * 点击添加站点
 * 显示/隐藏 站点表单和站点列表
 * 计算小车定位和姿态
 * 缓存小车姿态
 * **/
export function clickAddStation() {
    //订阅robot pose
    if (OBJ.robotPoseMsg) {
        let yawRad = quaternionToYaw(OBJ.robotPoseMsg.orientation);
        let yawDeg = (yawRad * 180 / Math.PI).toFixed(8);
        let infos = {
            positionX: OBJ.robotPoseMsg.position.x.toFixed(5),
            positionY: OBJ.robotPoseMsg.position.y.toFixed(5),
            positionZ: OBJ.robotPoseMsg.position.z.toFixed(5),
            orientationX: OBJ.robotPoseMsg.orientation.x.toFixed(5),
            orientationY: OBJ.robotPoseMsg.orientation.y.toFixed(5),
            orientationZ: OBJ.robotPoseMsg.orientation.z.toFixed(5),
            orientationW: OBJ.robotPoseMsg.orientation.w.toFixed(5),
            yaw: yawDeg
        }
        store.commit('setRobotPosition', infos)
    } else {
        // Message({
        //     showClose: true,
        //     message: '无法获取当前机器人姿态',
        //     type: 'error',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.error"),
            message: vue.$t("messages.robotPoseUnavailable"),
            type: 'error',
            position: 'top-left',
            offset: 100
        });
            
        return;
    }
    //隐藏站点列表
    store.commit('switchStationList', false);
    //显示添加站点
    store.commit('switchAddStation', true);
};

/**
 * 新建站点-提交
 * stationName:站点名称，stationType：站点类型，frameId
 * closeEnough：范围，timeout：超时，stationMode：站点模式
 * **/
export function submitAddStation(infos) {
    let stationName = infos.stationName;
    let stationType = infos.type;
    let frameId = infos.frameId;
    let closeEnough = parseFloat(infos.scope);
    let timeout = parseFloat(infos.timeout);
    let stationMode = infos.mode;
    if (!checkStr(stationName)) {
        // Message({
        //     showClose: true,
        //     message: '站点名称不合法',
        //     type: 'warning',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("messages.waypointNotValid"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
        return;
    }
    if (isNameUsedWaypoints(stationName)) {
        // Message({
        //     showClose: true,
        //     message: '该站点名称已存在',
        //     type: 'warning',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("messages.waypointExists"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
        return;
    }

    if (isNameUsedtraject(stationName)) {
        // Message({
        //     showClose: true,
        //     message: '该名称与轨迹名称重复',
        //     type: 'warning',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("messages.waypointWithTrajectoryExists"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
        return;
    }

    if (!frameId) {
        if (stationType === 'initial_pose' || stationType === 'goal') {
            // Message({
            //     showClose: true,
            //     message: '请输入frame_id',
            //     type: 'warning',
            //     duration: 3000
            // });
            Notification({
                title: vue.$t("titles.warning"),
                message: vue.$t("messages.frame_id"),
                type: 'warning',
                position: 'top-left',
                offset: 100
            });
            return;
        }
    }

    //如果是声音播放类型，切换站点模式
    if (stationType === 'sound_play') {
        stationMode = infos.voiceMode;
    }

    //如果是call类型，切换站点模式
    if (stationMode == 'call') {
        stationMode = infos.callMode;
    }

    //如果是goto类型，切换站点模式
    if (stationMode == 'goto') {
        stationMode = infos.gotoMode;
    }

    //判断站点模式是否为空
    if (!checkStr(stationMode)) {
        // Message({
        //     showClose: true,
        //     message: '站点模式不合法',
        //     type: 'error',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.error"),
            message: vue.$t("messages.waypointModeNotValid"),
            type: 'error',
            position: 'top-left',
            offset: 100
        });
        return;
    }

    let pose = {
        position: OBJ.robotPoseMsg.position,
        orientation: OBJ.robotPoseMsg.orientation
    };

    let waypointInfo = {
        stationName,
        stationType,
        stationMode,
        frameId,
        closeEnough,
        timeout,
        pose
    }

    let frameIdStr = {
        close_enough: waypointInfo.closeEnough,
        failure_mode: waypointInfo.stationMode,
        frame_id: waypointInfo.frameId,
        goal_timeout: waypointInfo.timeout,
        type: waypointInfo.stationType
    }
    var msg = new ROSLIB.Message({
        header: {
            frame_id: JSON.stringify(frameIdStr)
        },
        name: waypointInfo.stationName,
        pose: waypointInfo.pose,
        close_enough: 0.0,
        goal_timeout: 0.0,
        failure_mode: waypointInfo.stationMode,
    });
    OBJ.topic.addWaypointTopic.publish(msg);
    console.log('[INFO]waypoint added');
    // Message({
    //     showClose: true,
    //     message: '站点添加成功',
    //     type: 'success',
    //     duration: 3000
    // });
    Notification({
        title: vue.$t("titles.success"),
        message: vue.$t("messages.waypointAddSuccess"),
        type: 'success',
        position: 'top-left',
        offset: 100
    });
    hideOtherModule();
};

/**
 * 点击删除站点按钮
 * 判断是否在执行其他任务
 * 显示删除模态框
 * **/
export function delStationBtn() {
    hideOtherModule();
    store.commit('switchShowMask', true);
    store.commit('switchDeleteStation', true);
};

/**
 * 确定删除站点topic
 * **/
export function delStation(fullName) {
    var waypoint = getWaypointByName(fullName);//站点对象
    var msg = new ROSLIB.Message(waypoint);
    console.log(`delete Waypoint Name:${fullName}`)
    OBJ.topic.delWaypointTopic.publish(msg);
    viewer.scene.removeChild(waypoint);
    // Message({
    //     showClose: true,
    //     message: '站点删除成功',
    //     type: 'success',
    //     duration: 3000
    // });
    Notification({
        title: vue.$t("titles.success"),
        message:vue.$t("messages.WaypointDelSuccess"),
        type: 'success',
        position: 'top-left',
        offset: 100
    });
};

/**
 * 判断轨迹是否可用
 * **/
export function showTrack(boole) {
    if(!boole){
        store.state.commit('switchShowMask',false);
        store.state.commit('switchTrackList',false);
    }else{
        if (store.state.trajectoryList.length === 0) {
            OBJ.topic.trajectoriesTopic.subscribe((message) => {
                if (message.trajectories.length === 0) {
                    // Message({
                    //     showClose: true,
                    //     message: '暂无轨迹，请先添加轨迹',
                    //     type: 'error',
                    //     duration: 3000
                    // });
                    Notification({
                        title: vue.$t("titles.error"),
                        message: vue.$t("messages.noTrajectory"),
                        type: 'error',
                        position: 'top-left',
                        offset: 100
                    });
                }else{
                    store.commit('trajectoryList', message.trajectories);
                }
            });
        }
        store.commit('switchShowMask',true);
        store.commit('switchTrackList',true);
    }
};

/**
 * 点击轨迹显示轨迹详情
 * 设置轨迹详情和轨迹名称
 * **/
export function showTrackDetail(name) {
    hideOtherModule();
    store.commit('switchShowMask', true);
    store.commit('switchTrackModule', true);
    var tracks = getTrajectoryByName(name);
    store.commit('setTrackDetail', tracks);
};

/**
 * 点击导航指定轨迹
 * **/
export function navTrack(name) {
    var trajInfo = getTrajectoryByName(name);
    navCtrl(trajInfo.name, 1);
};


/**
 * 点击删除轨迹按钮
 * 显示模态框
 * 如果导航中 return
 * **/
export function deleteTrack(name) {
    hideOtherModule();
    store.commit('switchShowMask', true);
    store.commit('switchDeleteTrack', true);
};

/**
 * 确定删除轨迹
 * **/
export function deltrack(fullName) {
    var traj = getTrajectoryByName(fullName);
    var msg = new ROSLIB.Message(traj);
    console.log(`delete Trajectory Name:${traj}`)
    OBJ.topic.delTrajTopic.publish(msg);
    // Message({
    //     showClose: true,
    //     message: '轨迹删除成功',
    //     type: 'success',
    //     duration: 3000
    // });
    Notification({
        title: vue.$t("titles.success"),
        message: vue.$t("messages.trajectoryDelSuccess"),
        type: 'success',
        position: 'top-left',
        offset: 100
    });
};

/**
 * 新建轨迹
 * 提交
 * **/
export function submitAddTrack(infos) {
    if (infos.name === "") {
        // Message({
        //     showClose: true,
        //     message: '请完善轨迹名称',
        //     type: 'warning'
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("messages.refineTrajecName"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
        return
    }
    if (infos.stations.length === 0) {
        // Message({
        //     showClose: true,
        //     message: '请至少选择一个站点',
        //     type: 'warning'
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("messages.minSelectWaypoint"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
        return
    }
    if (isNameUsedtraject(infos.name)) {
        // Message({
        //     showClose: true,
        //     message: '该轨迹名称已存在',
        //     type: 'warning'
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("messages.trajectoryExists"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
        return
    }

    if (isNameUsedWaypoints(infos.name)) {
        // Message({
        //     showClose: true,
        //     message: '该名称与站点名称重复',
        //     type: 'warning',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("messages.trajectoryWithWaypointExists"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
        return;
    }

    let stationsName = infos.stations;
    let waypoints = [];
    for (let i = 0; i < stationsName.length; i++) {
        let selectedWpName = stationsName[i];
        waypoints.push(getWaypointByName(selectedWpName));
    }
    let msg = new ROSLIB.Message({
        name: infos.name,
        waypoints: waypoints
    });
    OBJ.topic.addTrajectoryTopic.publish(msg);
    // Message({
    //     showClose: true,
    //     message: '轨迹添加成功',
    //     type: 'success',
    //     duration: 3000
    // });
    Notification({
        title: vue.$t("titles.success"),
        message: vue.$t("messages.trajectoryAddSuccess"),
        type: 'success',
        position: 'top-left',
        offset: 100
    });
    hideOtherModule();
};

/**
 * 全局路径
 * **/
export function globalPlan(boole = true) {
    return new Promise(function(resolve, reject){
        if (boole) {
            var path = null;
            OBJ.topic.globalPathTopic.subscribe(function (data) {
                if (path != null) {
                    viewer.scene.removeChild(path);
                }
                if (data.poses.length > 0) {
                    path = new ROS2D.PathShape({
                        path: data,
                        strokeSize: 0.17,
                        strokeColor: createjs.Graphics.getRGB(69, 182, 69, 0.9),
                    });
                    OBJ.globalPlanStage = path;
                    viewer.scene.addChild(path);
                }
            });
        } else {
            OBJ.topic.globalPathTopic.unsubscribe();
            viewer.scene.removeChild(OBJ.globalPlanStage)
        }
        resolve()
    })
}

/**
 * 局部路径
 * **/
export function tabLocalPlan(boole = true) {
    return new Promise(function(resolve, reject){
        if (boole) {
            var path = null;
            OBJ.topic.tabLocalPathTopic.subscribe(function (data) {
                if (path != null) {
                    viewer.scene.removeChild(path);
                }
                if (data.poses.length > 0) {
                    path = new ROS2D.PathShape({
                        path: data,
                        strokeSize: 0.17,
                        strokeColor: createjs.Graphics.getRGB(245, 108, 108, 0.9),
                    });
                    OBJ.tabLocalPlanStage = path;
                    viewer.scene.addChild(path);
                }
            });
        } else {
            OBJ.topic.tabLocalPathTopic.unsubscribe();
            viewer.scene.removeChild(OBJ.tabLocalPlanStage);
        }
        resolve("local plan")
    })
}

/**
 * 控制放大
 *  一秒切换logo过度效果
 * **/
export function zoomIn(scale) {
    let scaleNum = Number(store.state.scale);
    store.commit("zoomDisabled", 2)
    if (scale === 'zoomIn') {
        store.commit('setScale', scaleNum *= 1.2);
        if (scaleNum > 2.5) {
            store.commit('zoomDisabled', 0);
            return
        }
        showWayPoints2();
        robotMarker();
        zoomview.startZoom(viewer.scene.x + OBJ.robotStage.x, viewer.scene.y + OBJ.robotStage.y);
        zoomview.zoom(1.2);
    } else {
        store.commit('setScale', scaleNum *= 0.8);
        if (scaleNum < 0.32) {
            store.commit('zoomDisabled', 1);
            return
        }
        showWayPoints2();
        robotMarker();
        zoomview.startZoom(viewer.scene.x + OBJ.robotStage.x, viewer.scene.y + OBJ.robotStage.y);
        zoomview.zoom(0.8);
    }

    store.commit('checkLogo', false)
    setTimeout(function () {
        store.commit('checkLogo', true)
    }, 1000);
};

function showWayPoints2(){
    var flag = false;
    if (OBJ.waypointsStage) {
        if (OBJ.waypointsStage) {
            for (var key in OBJ.waypointsStage) {
                var waypointsContainers = OBJ.waypointsStage[key];
                for (var i = 0; i < waypointsContainers.length; i++) {
                    viewer.scene.removeChild(waypointsContainers[i]);
                    flag = true;
                }
            }

            setTimeout(function(){
                if(flag){
                    var waypointContainers = {
                        goal: [],
                    };
                    for(var j = 0; j < OBJ.waypointsMessage.waypoints.length; j++){
                        //获取到站点信息之后赋给waypointsMsg，渲染站点，添加事件
                        var waypoint = OBJ.waypointsMessage.waypoints[j];
                        var wpInfo = JSON.parse(waypoint.header.frame_id);
                        var waypointType = wpInfo.type;
            
                        //如果不是目标站点，直接跳过
                        if (waypointType !== 'goal') {
                            continue;
                        }
                        var wpContainer = new ROS2D.NavigationArrow({
                            size: 10,
                            strokeSize: 1,
                            fillColor: createjs.Graphics.getRGB(63, 134, 254, 0.9),
                            pulse: false
                        });
                        wpContainer.x = waypoint.pose.position.x;
                        wpContainer.y = -waypoint.pose.position.y;
                        wpContainer.scaleX = 1.0 / viewer.scene.scaleX;
                        wpContainer.scaleY = 1.0 / viewer.scene.scaleY;
                        // change the angle
                        wpContainer.rotation = viewer.scene.rosQuaternionToGlobalTheta(waypoint.pose.orientation);
            
                        wpContainer.name = waypoint.name; // bind name
                        waypointContainers[waypointType].push(wpContainer);
                        // TODO: handle wp click
                        //点击站点导航到站点
                        wpContainer.on('click', waypointClick);
                        viewer.scene.addChild(wpContainer)
                        OBJ.waypointsStage = waypointContainers;
                        store.commit('stationList', waypointContainers.goal);
                    }
                }
            }, 200) 
        }  
    }
}



/**
 * 提交授权密码
 * 验证成功，授权缓存authorizationPassword
 * 通过检测缓存authorizationPassword来判断是否已授权
 * **/
export function submitAuthor(authorPassword) {
    if (authorPassword === store.state.authorizationPassword) {
        store.commit('switchAutherModule', false);
        store.commit('userRole', 'Administrator');
        var storage = window.localStorage;
        storage.setItem('authorizationPassword', authorPassword);
        storage.setItem('role', 'Administrator');
    } else if(authorPassword === store.state.developAuthorization){
        store.commit('switchAutherModule', false);
        store.commit('userRole', 'developer');
        var storage = window.localStorage;
        storage.setItem('authorizationPassword', authorPassword);
        storage.setItem('role', 'developer');
    } else {
        // Message({
        //     showClose: true,
        //     message: '密码错误，请重新输入',
        //     type: 'error'
        // });
        Notification({
            title: vue.$t("titles.error"),
            message: vue.$t("messages.pswError"),
            type: 'error',
            position: 'top-left',
            offset: 100
        });
        var storage = window.localStorage;
        storage.removeItem('authorizationPassword');
    }
};

/**
 * 调节声音
 * **/
export function adjustVoice(num) {
    let message = {
        seq: num,
        frame_id: 'system_volume'
    }
    var voiceMessage = new ROSLIB.Message(message);
    OBJ.topic.voiceTopic.publish(voiceMessage);
};

/**
 * 提交更新
 * **/
export function submitupdate(type, info) {
    var topic = '';
    switch (type) {
        case 'online':
            var msg = new ROSLIB.Message({
                data: info
            });
            OBJ.topic.cmdTopic.publish(msg);
            return;
        case 'offline':
            topic = '/shell_string';
            break;
        case 'dbparam':
            topic = '/system_shell/shell_string';
            break;
        case 'openssh':
            topic = '/shell_string';
            break;
        default:
            console.error(`[ERROR]unknown update mode: ${type}`);
            return;
    }
    var message = new ROSLIB.Message({
        data: info
    });
    var updateTopic = new ROSLIB.Topic({
        ros: ros,
        name: topic,
        messageType: 'std_msgs/String'
    });
    updateTopic.publish(message);
};

/**
 * 提交WiFi设置
 * **/
export function subWifiSet(options) {
    let ssid = options.ssid;
    let password = options.password;
    let ip = options.ip;
    let mask = options.mask;
    let geteway = options.gateway;
    let checked = options.checked;
    if (!ssid || !password) {
        // Message({
        //     showClose: true,
        //     message: 'wifi名称或密码不合法',
        //     type: 'error',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.error"),
            message: vue.$t("messages.wifiNameOrPswUnavailable"),
            type: 'error',
            position: 'top-left',
            offset: 100
        });
        return;
    }

    if (!checkIp(ip)) {
        // Message({
        //     showClose: true,
        //     message: 'ip不合法',
        //     type: 'error',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.error"),
            message: vue.$t("messages.ipUnavailable"),
            type: 'error',
            position: 'top-left',
            offset: 100
        });
        return;
    }

    if (mask === '') {
        // Message({
        //     showClose: true,
        //     message: '子掩码不合法',
        //     type: 'error',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.error"),
            message: vue.$t("messages.ipMaskUnavailable"),
            type: 'error',
            position: 'top-left',
            offset: 100
        });
        return;
    }

    if (geteway === '') {
        // Message({
        //     showClose: true,
        //     message: '网关不合法',
        //     type: 'error',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.error"),
            message: vue.$t("messages.gatewayUnavailable"),
            type: 'error',
            position: 'top-left',
            offset: 100
        });
        return;
    }

    //十进制转换为二进制，数字相加
    let num = 0;
    if (mask) {
        let arrMask = mask.split(".");
        for (let i = 0; i < arrMask.length; i++) {
            let arrConvert = Number(arrMask[i]).toString(2).split("");
            for (let j = 0; j < arrConvert.length; j++) {
                num = num + Number(arrConvert[j])
            }
        }
    }

    options['mode'] = PARAMS.NetworkMode.wifi;
    // var optionStr = JSON.stringify(options);
    var optionStr = checked ? `~/catkin_ws/www/shell/comm.sh -m wifi -s ${ssid} -p ${password} -i ${ip} -k ${num} -g ${geteway} -a` : `~/catkin_ws/www/shell/comm.sh -m wifi -s ${ssid} -p ${password} -i ${ip} -k ${num} -g ${geteway}`
    var msg = new ROSLIB.Message({
        data: optionStr
    });
    OBJ.topic.netWorkTopic.publish(msg);
    var storage = window.localStorage;
    storage.setItem('wifiInfos', JSON.stringify(options));
    store.commit('switchWifiSetting', false);
    Loading.service({
        text: vue.$t("inTheSwitch")
    })
    setTimeout(function () {
        window.location.href = `http://${ip}`;
        Loading.service().close();
    }, 10000);
};

/**
 * wifi复位
 * **/
export function wifiRest(options) {
    options['mode'] = PARAMS.NetworkMode.ap;
    // var optionStr = JSON.stringify(options);
    var optionStr = `~/catkin_ws/www/shell/comm.sh -m ap`;
    var msg = new ROSLIB.Message({
        data: optionStr
    });
    OBJ.topic.netWorkTopic.publish(msg);
    store.commit('switchWifiSetting', false);
    // reconnect
    Loading.service({
        text: vue.$t("inTheSwitch")
    })
    setTimeout(function () {
        window.location.href = `http://10.42.0.1`;
        Loading.service().close();
    }, 3000);
    store.commit('switchWifiSetting', false);
};

/**
 * 控制小车移动方向
 * 0：停止
 * **/
export function manualCtrl(info) {
    var linear = info.linear || 0;
    var angular = info.angular || 0;
    var msg = new ROSLIB.Message({
        linear: {
            x: linear,
            y: 0,
            z: 0
        },
        angular: {
            x: 0,
            y: 0,
            z: angular
        }
    });
    if (OBJ.manualCtrlTimer) {
        clearInterval(OBJ.manualCtrlTimer);
        OBJ.manualCtrlTimer = null;
    }
    if (linear === 0 && angular === 0) {
        OBJ.topic.cmdVelTopic.publish(msg);
        return;
    }
    
    OBJ.manualCtrlTimer = setInterval(function () {
        OBJ.topic.cmdVelTopic.publish(msg);
    }, 200);
}

/**
 * 点击PLC
 * **/
export function showPlc() {
    // Message({
    //     showClose: true,
    //     message: '正在开发中...',
    //     type: 'warning',
    //     duration: 3000
    // });
    Notification({
        title: vue.$t("titles.warning"),
        message: vue.$t("messages.beingDeveloped"),
        type: 'warning',
        position: 'top-left',
        offset: 100
    });
    return;
};


/**
 * display laserScan
 * **/
export function displayLaserScan(boole, options) {
    if (boole) {
        OBJ.topic.laserScanTopic.subscribe((message) => {
            OBJ.laserScanMsg = message;
            if (OBJ.laserScanStage) {
                viewer.scene.removeChild(OBJ.laserScanStage);
            }

            //激光坐标
            let laserScanPose = {
                x: OBJ.tfMsg.map2base_laser.transform.translation.x,
                y: -OBJ.tfMsg.map2base_laser.transform.translation.y,
                rotation: viewer.scene.rosQuaternionToGlobalTheta(OBJ.tfMsg.map2base_laser.transform.rotation)
            }
            OBJ.laserScanPose = laserScanPose;
            let skipNum = 10;
            let laserScanArr = [];
            for (var i = 0; i < message.ranges.length; i += skipNum) {
                if (message.ranges[i] === 'inf' || message.ranges[i] === 'nan'
                    || message.ranges[i] < message.range_min
                    || message.ranges[i] > message.range_max) {
                    continue;
                }
                var angle = message.angle_min + message.angle_increment * i;//激光线间隔弧度
                var rotationPI = laserScanPose.rotation / 180 * Math.PI;//激光弧度
                //激光线弧度
                let degree = -angle + rotationPI;
                //Math.cos转换为角度，根据角度计算边长
                let baseScanX = Math.cos(degree) * message.ranges[i];
                let baseScanY = Math.sin(degree) * message.ranges[i];
                //加激光坐标，得出激光点坐标
                let mapScanX = laserScanPose.x + baseScanX;
                let mapScanY = laserScanPose.y + baseScanY;
                laserScanArr.push({
                    x: mapScanX,
                    y: mapScanY
                })
            }
            OBJ.laserScanStage = laserScanShape(laserScanArr, {
                fillColor: '#ff9800'
            });
            viewer.scene.addChild(OBJ.laserScanStage);
        })
    } else {
        OBJ.topic.laserScanTopic.unsubscribe();
        viewer.scene.removeChild(OBJ.laserScanStage);
    }
}

//激光绘制
function laserScanShape(laserScanMessage, options) {
    var container = new createjs.Container();
    var size = 0.05;
    var fillColor = createjs.Graphics.getRGB(255, 153, 0, 1);
    var graphics = new createjs.Graphics();
    graphics.beginFill(fillColor);
    for (var i = 0; i < laserScanMessage.length; i++) {
        var posRos = laserScanMessage[i];
        graphics.moveTo(posRos.x, posRos.y);
        graphics.drawCircle(posRos.x, posRos.y, size);
    }
    graphics.endFill();
    var shape = new createjs.Shape(graphics);
    container.addChild(shape);
    return container;
}


/**
 * 显示、隐藏轮廓
 * **/
export function displayFootprint(boole) {
    if (boole) {
        OBJ.topic.footprintTopic.subscribe(message => {
            if (OBJ.footprintStage) {
                viewer.scene.removeChild(OBJ.footprintStage);
            }
            OBJ.footprintStage = footprintShape(message, {
                strokeColor: '#8bc34a'
            });
            viewer.scene.addChild(OBJ.footprintStage);
        })
    } else {
        OBJ.topic.footprintTopic.unsubscribe();
        viewer.scene.removeChild(OBJ.footprintStage);
    }
}

//轮廓绘制
function footprintShape(footprint, options) {
    var container = new createjs.Container();
    if (footprint.polygon.points.length < 3) {
        return container;
    }
    var strokeSize = options.strokeSize || 0.05;
    var strokeColor = options.strokeColor || createjs.Graphics.getRGB(0, 255, 0, 1);
    var graphics = new createjs.Graphics();
    graphics.setStrokeStyle(strokeSize);
    graphics.beginStroke(strokeColor);
    var posRos = footprint.polygon.points[0];
    // var posPx = viewer.scene.rosQuaternionToGlobalTheta(posRos);
    var posPxOrg = posRos;
    graphics.moveTo(posRos.x, -posRos.y);
    for (var i = 1; i < footprint.polygon.points.length; i++) {
        posRos = footprint.polygon.points[i];
        graphics.lineTo(posRos.x, -posRos.y);
        graphics.moveTo(posRos.x, -posRos.y);
    }
    graphics.lineTo(posPxOrg.x, -posPxOrg.y);
    graphics.endStroke();
    var shape = new createjs.Shape(graphics);
    container.addChild(shape);
    return container;
}



/**
 * 正则匹配特殊符号
 * */
export function checkStr(str) {
    var myReg = /^[^@\/\'\\\"\‘\’#$%&\^\*]+$/;
    return myReg.test(str);
}

/**
 * 检索ip是否匹配
 * **/
function checkIp(ipStr) {
    //match字符串内检索指定的值
    ipStr = ipStr.match(/(\d+)\.(\d+)\.(\d+)\.(\d+)/g);
    if (ipStr == null) {
        return false;
    }
    //正则匹配的第几个子匹配字符串
    else if (RegExp.$1 > 255 || RegExp.$2 > 255 || RegExp.$3 > 255 || RegExp.$4 > 255) {
        return false;
    }
    else {
        return true;
    }
}

/**
 * 判断用户是否授权
 * **/
export function verifyAuthorization(){
    if(!window.localStorage.getItem('authorizationPassword')){
        // Message({
        //     showClose: true,
        //     message: '暂无操作权限，请输入授权密码',
        //     type: 'warning'
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("messages.notPermission"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
        return false;
    }
    if(window.localStorage.getItem('role') === "developer"){
        store.commit("userRole", "develop");
    }
    return true
}

/**
 * 判断导航状态是否正在执行其他任务
 * **/
export function verifyNavCtrlStatus() {
    if (OBJ.navCtrlStatus.status !== PARAMS.NavCtrlStatus.idling && OBJ.navCtrlStatus.status !== PARAMS.NavCtrlStatus.cancelled) {
        // Message({
        //     showClose: true,
        //     message: '正在执行其他任务，当前命令将被忽略',
        //     type: 'warning',
        //     duration: 3000
        // });
        Notification({
            title: vue.$t("titles.warning"),
            message: vue.$t("messages.tasksBeing"),
            type: 'warning',
            position: 'top-left',
            offset: 100
        });
        return false;
    }
    return true;
}

/**
 * 站点名字是否重复
 * **/
export function isNameUsedWaypoints(name) {
    if (OBJ.wayPointsMsg) {
        for (var i = 0; i < OBJ.wayPointsMsg.waypoints.length; i++) {
            if (OBJ.wayPointsMsg.waypoints[i].name === name) {
                return true;
            }
        }
    }
    return false;
};

/**
 * 轨迹名字是否重复
 * **/
export function isNameUsedtraject(name) {
    if (store.state.trajectoryList) {
        for (var i = 0; i < store.state.trajectoryList.length; i++) {
            if (store.state.trajectoryList[i].name === name) {
                return true;
            }
        }
    }
    return false;
};

/**
 * 通过名称找到对应站点数据
 * **/
export function getWaypointByName(name) {
    for (var i = 0; i < OBJ.wayPointsMsg.waypoints.length; i++) {
        var waypoint = OBJ.wayPointsMsg.waypoints[i];
        if (waypoint.name === name) {
            return waypoint;
        }
    }
}


/**
 * 通过名称获取对应的轨迹数据
 * **/
export function getTrajectoryByName(name) {
    var traj;
    for (var i = 0; i < store.state.trajectoryList.length; i++) {
        traj = store.state.trajectoryList[i];
        if (traj.name === name) {
            break;
        }
    }
    return traj;
};

//quaternion > yaw
export function quaternionToYaw(orientation, ignoreXY) {
    var rotation = orientation;
    var numerator;
    var denominator;
    if (ignoreXY) {
        numerator = 2 * rotation.w * rotation.z;
        denominator = 1 - 2 * Math.pow(rotation.z, 2);
    } else {
        numerator = 2 * (rotation.w * rotation.z + rotation.x * rotation.y);
        denominator = 1 - 2 * (Math.pow(rotation.y, 2) + Math.pow(rotation.z, 2));
    }
    var yaw = Math.atan2(numerator, denominator);
    return yaw;
};


/**
 * 有效的四元数
* **/
export function isQuaternionValid(orientation, threshold) {
    var threshold = threshold || 0.0001;
    var sum = 0;
    for (var key in orientation) {
        if (orientation.hasOwnProperty(key)) {
            sum += orientation[key] * orientation[key];
        }
    }
    return (Math.abs(sum - 1) < threshold);
}


/**
 * 转换为四元数
 * **/
export function thetaToQuaternion(theta) {
    return {
        x: 0,
        y: 0,
        z: Math.sin(theta / 2),
        w: Math.cos(theta / 2)
    };
};

/**
 * 点击地图隐藏其他模态框
 * type:相对应的不处理
 * **/
export function hideOtherModule(type) {
    switch (type) {
        default:
            store.commit('switchAutherModule', false)
            store.commit('switchMapList', false);
            store.commit('switchDelMap', false);
            store.commit('switchMoreBtns', false);
            store.commit('switchDragModule', false);
            store.commit('switchWifiSetting', false);
            store.commit('switchAddStation', false);
            store.commit('switchStationList', false);
            store.commit('switchTrackList', false);
            store.commit('switchAddMap', false);
            store.commit('switchStationModule', false);
            store.commit('switchDeleteStation', false);
            store.commit('switchStationEdit', false);
            store.commit('switchDelMap', false);
            store.commit('switchTrackModule', false);
            store.commit('switchDeleteTrack', false);
            store.commit('switchAddTrack', false);
            store.commit('switchShowMask', false);
            store.commit('switchUpdate', false);
            store.commit('switchVoice', false);
    }
}