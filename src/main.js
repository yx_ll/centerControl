// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUi from 'element-ui'
import VueI18n from 'vue-i18n'
import 'element-ui/lib/theme-chalk/index.css';
import App from './App'
import router from './router'
import store from './store'
import echarts from 'echarts'
import axios from 'axios'
import _ from 'lodash'
import fastclick from 'fastclick'



import '@/css/main.css'
// import initJs from './js/init'

fastclick.attach(document.body)
Vue.filter('numFilter',function (value) {
  let realVal = Number(value).toFixed(2);
  return Number(realVal)
});

Vue.config.productionTip = false

Vue.use(ElementUi)
Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'cn',//默认语言
  messages: {
    'cn': require('./lang/cn'),
    'en': require('./lang/en')
  }
})

const deBug = new debugout()

Vue.filter('robotsName', function(val){
  return val.slice(1,6)
})

Vue.prototype.getDate = function(){
  let date = new Date();
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();
  if(hours < 10){
    hours = "0" + hours
  }
  if(minutes < 10){
    minutes = "0" + minutes
  }
  if(seconds < 10){
    seconds = "0" + seconds
  }
  let HHMMSS = `${hours} :  ${minutes} : ${seconds}`
  return HHMMSS
  
}

Vue.prototype.$debugout = deBug
Vue.prototype.$echarts = echarts

/* eslint-disable no-new */
var vue = new Vue({
  el: '#app',
  router,
  store,
  i18n,
  deBug,
  axios,
  _,
  components: { App },
  template: '<App/>'
})

export default vue