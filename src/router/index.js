import Vue from 'vue'
import Router from 'vue-router'
const Home = resolve => {
  resolve(require('@/components/home'))
}
const Login = resolve => {
  resolve(require('@/components/login'))
}
const CenterControl = resolve => {
  resolve(require('@/components/centerControl/centerControl'))
}
Vue.use(Router)
const router = new Router({
  // mode: 'history',
  routes:[
    {
      path: '/',
      redirect:'/login'
    },
    {
      path :'*',//404匹配未找到的页面
      component : Login
    },
    {
      path: '/login',
      name:"login",
      component: Login,
      meta:{
        keepAlive:false
      }
    },
    {
      path: '/home',
      name:"home",
      component: Home,
      meta:{
        keepAlive:false
      }
    },
    {
      path: '/centerControl/centerControl',
      name: "centerControl",
      component: CenterControl,
      meta:{
        keepAlive:false
      }
    }
  ]
})
export default router
