import Vue from 'vue'
import vuex from 'vuex'
Vue.use(vuex)
//获取store:this.$store.state.xxx
export default new vuex.Store({
    state:{
        swjIP: "",
        centerControlIP: "",
        showMapList: false,
        mapList: [],
        wayPointMsg: "",
        stationList:[],//站点列表
        deleteStation:false,//站点删除模态框
        stationName: "",//站点名称
        showMask:false,//遮罩
        robotPosition: '',//机器人坐标
        stationsModule: false,//站点列表模态框
        showAddStation:false,//添加站点表单
        trajectoryList: [],//轨迹列表
        tracksModule:false,//轨迹模态框
        trackModule: false,//轨迹详情模态框
        trackDetail: '',//轨迹详情
        delTrackName: '',//删除轨迹的名称
        deleteTrack: false,//轨迹删除模态框
        showAddTrack: false,//添加轨迹表单
        scale: 1,//缩放比例
        centerScale: 1,
        zoomDisabled: 2,//0：max  1:min  2:null
        centerZoomDisabled:2,
        rosMode: "",//ros状态
        mappingFlag: false,
        chargeStatus: null,//充电状态
        showBattery: false,
        batteryBg:"#3f86fe",
        battery: 100,//电量
        authorization: false,//授权模态框
        authorizationPassword: '6022',
        developAuthorization: "343",
        role: "",
        userRole: null,
        batteryStatus: null,
        batteryState: "",
        edipMapModule: false,//编辑地图模态框
        wifiInfos: '',//wifi数据
        showWifiSetting:false,//wifi设置
        showLogo: true,//logo
        addMap: false,//添加地图
        showUpdate: false,//更新
        showVoice: false,//音量调节
        nowMapName: 'MAP',//当前地图名称
        delMap:false,//地图删除模态框
        delMapName:'',//要删除的地图名称
        disabledMoveMap: false,//地图移动
        stationModule:false,//站点详情模态框
        stationName:'',//站点名称
        stationEdit: false,//站点编辑模态框
        showMoreBtns:false,//更多按钮
        showDragModule: false,//拖拽
        centerLogs: [],//日志
        centerCollapseName: "robots",
        centerWaypointsActiveName: "",
        robots: [
            {name: "robot_001",ip: "10.42.0.1",connect: true,charge: true,power: 10,runMessage: "运行中"},
            {name: "robot_002",ip: "10.42.0.1",connect: true,charge: false, power: 20,runMessage: "运行中2"},
            {name: "robot_003",ip: "10.42.0.1",connect: false,charge: true, power: 30,runMessage: "运行中3"},
            {name: "robot_004",ip: "10.42.0.1",connect: true,charge: true, power: 40,runMessage: "运行中4"},
            {name: "robot_005",ip: "10.42.0.1",connect: true,charge: true, power: 50,runMessage: "运行中5"},
            {name:"",ip: "10.42.0.1",connect: true,charge: true, power: 60,runMessage: "运行中6"}
        ],//控制中心所有robot
        centerWaypoints: [],//站点列表
    },
    mutations:{
        setSwjIP(state, ip){
            state.swjIP = ip
        },

        setCenterIP(state, ip){
            state.centerControlIP = ip
        },

        switchMapList(state, type){
            state.showMapList = type;
        },

        mapList(state, list){
            state.mapList = list
        },

        wayPointMsg(state, message){
            state.wayPointMsg = message;
        },
        
        //站点列表list
        stationList(state, type = []){
            state.stationList = type
        },

        //显示/隐藏站点列表模态框
        switchStationList(state, type){
            state.stationsModule = type
        },

        //显示/隐藏添加站点
        switchAddStation(state, type){
            state.showAddStation = type
        },

        //站点删除
        switchDeleteStation(state,type){
            state.deleteStation = type
        },

        //显示遮罩
        switchShowMask(state,type){
            state.showMask = type
        },

        //添加站点，机器人坐标
        setRobotPosition(state,info){
            state.robotPosition = info;
        },

        //删除站点名称
        setStationName(state, name){
            state.stationName = name
        },

        //轨迹列表
        trajectoryList(state, type = []){
            state.trajectoryList = type;
        },

        //显示/隐藏添加轨迹
        switchTrackList(state,type){
            state.tracksModule = type;
        },

        //显示/隐藏轨迹详情模态框
        switchTrackModule(state,type){
            state.trackModule = type;
        },

        //删除轨迹的名称
        delTrackName(state, name){
            state.delTrackName = name;
        },

        //轨迹删除模态框
        switchDeleteTrack(state,type){
            state.deleteTrack = type;
        },

        //显示/隐藏添加轨迹
        switchAddTrack(state,type){
            state.showAddTrack = type;
        },

        //缩放比例
        setScale(state, num){
            state.scale = num;
        },

        //控制中心scale
        setCenterScale(state, num){
            state.centerScale = num;
        },

        //设置缩放disabled按钮
        zoomDisabled(state, type){
            state.zoomDisabled = type
        },

        centerZoomDisabled(state, type){
            state.centerZoomDisabled = type;
        },

        //设置ros状态
        setRosMode(state, type){
            state.rosMode = type;
        },

        mappingFlag(state, type){
            state.mappingFlag = type
        },

        //充电状态
        chargeStatus(state, type){
            state.chargeStatus = type;
        },

        showBattery(state, type){
            state.showBattery = type
        },

        //切换电池状态
        switchBatteryStatus(state,type){
            state.batteryStatus = type;
        },

        //编辑地图按钮组模态框
        switchEditMapBtns(state,type){
            state.edipMapModule = type;
        },

        //显示/隐藏设置权限
        switchAutherModule(state,type){
            state.authorization = type;
        },

        //用户角色
        userRole(state, user){
            state.userRole = user;
        },

        //电量颜色
        setBatteryBg(state,type){
            state.batteryBg = type
        },

        //电量
        subButtery(state,type){
            state.battery = type
        },

        //电量状态
        batteryState(state, text){
            state.batteryState = text;
        },

        //设置wifi
        setWifiInfos(state,infos){
            state.wifiInfos = infos
        },

        //切换wifi设置
        switchWifiSetting(state,type){
            state.showWifiSetting = type
        },

        //放大缩小切换logo
        checkLogo(state,type){
            state.showLogo = type
        },

        //显示/隐藏添加地图
        switchAddMap(state,type){
            state.addMap = type
        },

        //显示/隐藏更新
        switchUpdate(state, type){
            state.showUpdate = type
        },

        //切换音量调节
        switchVoice(state,type){
            state.showVoice = type
        },

        //当前地图名称
        setNowMapName(state,type){
            state.nowMapName = type;
        },

        //地图删除模态框
        switchDelMap(state,type){
            state.delMap = type;
        },

        //要删除的地图名称
        setDelMapName(state,info){
            state.delMapName = info;
        },

        //禁止和开启移动地图
        switchMoveMap(state,type){
            state.disabledMoveMap = type;
        },

        //站点详情模态框
        switchStationModule(state, type){
            state.stationModule = type;
        },

        //站点编辑
        switchStationEdit(state,type){
            state.stationEdit = type
        },
        
        //设置轨迹详情
        setTrackDetail(state,info){
            state.trackDetail = info
        },

        //切换更多按钮
        switchMoreBtns(state,type = false){
            state.showMoreBtns = type
        },

        //切换拖拽模态框
        switchDragModule(state,type = false){
            state.showDragModule = type
        },

        //放大缩小比例
        mapScale(state,type){
            state.scale = type
        },

        //日志
        setCenterLogs(state, msg){
            state.centerLogs.push(msg)
        },

        //waypoints
        centerWaypoints(state, arr){
            state.centerWaypoints = arr
        },

        //折叠面板
        centerCollapseName(state, type){
            state.centerCollapseName = type
        },

        //站点折叠面板
        centerWaypointsActiveName(state, type){
            state.centerWaypointsActiveName = type
        },

        //设置机器人列表
        setRobotsInfo(state, info){
            state.robots = info
        }

       
    }
})